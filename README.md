## NODEJS, NESTJS, Typescript, MongoDB, Firebase FCM

This is a real-time chat app using NodeJS and ReactJS
It’s accompanied with a ReactJs repo: https://gitlab.com/hoangsonnguyen/chat-app

## install on Mac/Linux OS
```
sh
npm install
```
please use sudo if necessary

## install on Windows OS
```
sh
npm install
```

## start
```
sh
npm run start:dev
```
## if you have any trouble with DB connection after a while, please ping me at sonnguyeninslife@gmail.com 
