import * as dotenv from 'dotenv';
import * as Joi from 'joi';
import * as fs from 'fs';
import { extname } from 'path';
import { diskStorage } from 'multer';

export interface EnvConfig {
  [key: string]: string;
}

export class ConfigService {
  private readonly envConfig: EnvConfig;

  constructor() {
    const nodeEnv = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';
    const filePath = `.${nodeEnv}.env`;
    const config = dotenv.parse(fs.readFileSync(filePath));
    config.NODE_ENV = nodeEnv;
    this.envConfig = this.validateInput(config);
  }

  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid(['development', 'production', 'test', 'provision'])
        .default('development'),
      DATABASE_USER: Joi.string().required(),
      DATABASE_PASSWORD: Joi.string().required(),
      DATABASE_HOST: Joi.string().required(),
      DATABASE_PORT: Joi.number().required(),
      DATABASE_NAME: Joi.string().required(),
      APP_HOST: Joi.string().required(),
      APP_PORT: Joi.number().required(),
      JWT_SECRET: Joi.string().required(),
      BCRYPT_SALT: Joi.number().required(),
      FIREBASE_TYPE: Joi.string().required(),
      FIREBASE_PROJECTID: Joi.string().required(),
      FIREBASE_PRIVATEKEY_ID: Joi.string().required(),
      FIREBASE_PRIVATEKEY: Joi.string().required(),
      FIREBASE_CLIENT_EMAIL: Joi.string().required(),
      FIREBASE_CLIENT_ID: Joi.string().required(),
      FIREBASE_AUTH_URI: Joi.string().required(),
      FIREBASE_TOKEN_URI: Joi.string().required(),
      FIREBASE_AUTH_PROVIDER_X509_CERT_URL: Joi.string().required(),
      FIREBASE_CLIENT_X509_CERT_URL: Joi.string().required(),
      FIREBASE_SERVER_KEY: Joi.string().required(),
      FIREBASE_TOKENINFO_URI: Joi.string().required(),
      CAPTCHA_SITE_KEY: Joi.string().required(),
      CAPTCHA_SECRET_KEY: Joi.string().required(),
      CAPTCHA_VERIFY_URL: Joi.string().required(),
      REGEX: Joi.string().required(),
      JWT_COMFIRM_SECRET: Joi.string().required(),
      SMTP_HOST: Joi.string().required(),
      SMTP_PORT: Joi.number().required(),
      SMTP_USER: Joi.string().required(),
      SMTP_PASSWORD: Joi.string().required(),
      SMTP_SENDER_DEFAULT: Joi.string().required(),
    });

    const { error, value: validatedEnvConfig } = Joi.validate(
      envConfig,
      envVarsSchema,
    );

    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }

    const jwtComfirmSecret = String(validatedEnvConfig.JWT_COMFIRM_SECRET);
    const jwtSecret = String(validatedEnvConfig.JWT_SECRET);

    if (jwtComfirmSecret === jwtSecret) {
      throw new Error('Jwt secret key and jwt comfirm secret key must be different');
    }

    return validatedEnvConfig;
  }

  get nodeEnv(): string {
    return String(this.envConfig.NODE_ENV);
  }

  get host(): string {
    return String(this.envConfig.APP_HOST);
  }

  get port(): string {
    return String(this.envConfig.APP_PORT);
  }

  get jwtSecret(): string {
    return String(this.envConfig.JWT_SECRET);
  }

  get bcryptSalt(): number {
    return Number(this.envConfig.BCRYPT_SALT);
  }

  get dbURI(): string {
    // tslint:disable-next-line:max-line-length
    return `mongodb://${this.envConfig.DATABASE_USER}:${this.envConfig.DATABASE_PASSWORD}@${this.envConfig.DATABASE_HOST}:${this.envConfig.DATABASE_PORT}/${this.envConfig.DATABASE_NAME}?ssl=true`;
  }

  get smtpHost(): string {
    return String(this.envConfig.SMTP_HOST);
  }

  get smtpPort(): number {
    return Number(this.envConfig.SMTP_PORT);
  }

  get smtpUser(): string {
    return String(this.envConfig.SMTP_USER);
  }

  get smtpPassword(): string {
    return String(this.envConfig.SMTP_PASSWORD);
  }

  get smtpSenderDefault(): string {
    return String(this.envConfig.SMTP_SENDER_DEFAULT);
  }

  get captchaSiteKey(): string {
    return String(this.envConfig.CAPTCHA_SITE_KEY);
  }

  get captchaSecretKey(): string {
    return String(this.envConfig.CAPTCHA_SECRET_KEY);
  }

  get captchaVerifyUrl(): string {
    return String(this.envConfig.CAPTCHA_VERIFY_URL);
  }

  get getRegex(): string {
    return String(this.envConfig.REGEX);
  }

  get getJwtComfirmSecret(): string {
    return String(this.envConfig.JWT_COMFIRM_SECRET);
  }

  get multerOptions(): any {
    return {
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, cb) => {
          const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');
          return cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
    };
  }

  get firebaseType(): string {
    return String(this.envConfig.FIREBASE_TYPE);
  }

  get firebaseProjectId(): string {
    return String(this.envConfig.FIREBASE_PROJECTID);
  }

  get firebasePrivateKeyId(): string {
    return String(this.envConfig.FIREBASE_PRIVATEKEY_ID);
  }

  get firebasePrivateKey(): string {
    return String(this.envConfig.FIREBASE_PRIVATEKEY);
  }

  get firebaseClientEmail(): string {
    return String(this.envConfig.FIREBASE_CLIENT_EMAIL);
  }

  get firebaseClientId(): string {
    return String(this.envConfig.FIREBASE_CLIENT_ID);
  }

  get firebaseAuthURI(): string {
    return String(this.envConfig.FIREBASE_AUTH_URI);
  }

  get firebaseTokenURI(): string {
    return String(this.envConfig.FIREBASE_TOKEN_URI);
  }

  get firebaseAuthProviderX509CertUrl(): string {
    return String(this.envConfig.FIREBASE_AUTH_PROVIDER_X509_CERT_URL);
  }

  get firebaseClientX509CertUrl(): string {
    return String(this.envConfig.FIREBASE_CLIENT_X509_CERT_URL);
  }

  get firebaseServerKey(): string {
    return String(this.envConfig.FIREBASE_SERVER_KEY);
  }

  get firebaseTokenInfoURI(): string {
    return String(this.envConfig.FIREBASE_TOKENINFO_URI);
  }
}
