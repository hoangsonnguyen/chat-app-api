import { ForbiddenException } from '@nestjs/common';
import { checkArrayObjectIDs } from '../helpers/check';
import { Types } from 'mongoose';

export interface ValidateAccess {
  data: any;
  credentials: Credentials;
  authzKey?: string;
}

export interface Credentials {
  _id: Types.ObjectId;
  email?: string;
}

export const validateAccessToSingle = ({ data, credentials, authzKey}: ValidateAccess) => {
  if (!data || Array.isArray(data)) {
    return {
      validData: null,
    };
  }

  const { _id: userID } = credentials;

  const authzData = authzKey
    ? data[authzKey]
    : data;

  const isOwned = authzData._id && userID === authzData._id.toString();

  if (isOwned) {
    return {
      validData: data,
    };
  }

  const isMember = authzData.users && checkArrayObjectIDs({
    requireIDs: authzData.users,
    neededCheckIDs: [userID],
  });

  if (isMember) {
    return {
      validData: data,
    };
  }

  throw new ForbiddenException('No permissions');
};

export const validateAccessToList = ({ data, credentials, authzKey }: ValidateAccess) => {
  if (!data.length) {
    return {
      validData: [],
      validDataLength: 0,
    };
  }

  const { _id: userID } = credentials;

  let validDataLength = 0;

  const validData = data.map((each) => {
    const authzData = authzKey
      ? each[authzKey]
      : each;
    const isOwned = authzData._id && userID === authzData._id.toString();

    if (isOwned) {
      ++validDataLength;

      return each;
    }

    const isMember = authzData.users && checkArrayObjectIDs({
      requireIDs: authzData.users,
      neededCheckIDs: [userID],
    });

    if (isMember) {
      ++validDataLength;

      return each;
    }

    return null;
  });

  return {
    validData,
    validDataLength,
  };
};
