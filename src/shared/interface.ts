import { Credentials } from './validateAccess';

export interface FindOne {
  query: object;
  credentials: Credentials;
}
