import { Module, forwardRef } from '@nestjs/common';
import { UsersModule } from '../users/users.module';
import { MessagesController } from './messages.controller';
import { MessagesService } from './messages.service';
import { FireBaseModule } from '../firebase/firebase.module';
import { MongooseModule } from '@nestjs/mongoose';
import { MessagesSchema } from './models/messages.schema';
import { RoomsModule } from '../rooms/rooms.module';
import { NotificationsModule } from '../notifications/notifications.module';

const MessagesModel = MongooseModule.forFeature([{ name: 'Messages', schema: MessagesSchema }]);

@Module({
  imports: [
    MessagesModel,
    UsersModule,
    FireBaseModule,
    NotificationsModule,
    forwardRef(() => RoomsModule),
  ],
  controllers: [MessagesController],
  providers: [MessagesService],
  exports: [MessagesService, MessagesModel],
})
export class MessagesModule { }
