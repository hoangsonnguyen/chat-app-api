import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { FindOne } from '../../shared/interface';
import { Message, DeleteService, FindAll, FindManyService, CreateService, UpdateService } from './types/messages.interface';
import { buildFindingQuery } from '../../helpers/build';
import { getNextCursor } from '../../helpers/get';
import { RoomsService } from '../rooms/rooms.service';
import { validateAccessToList, validateAccessToSingle } from '../../shared/validateAccess';

@Injectable()
export class MessagesService {
  constructor(
    @Inject(forwardRef(() => RoomsService)) private readonly roomsService: RoomsService,
    @InjectModel('Messages') private readonly messagesModel: Model<Message>,
  ) { }

  private validateAccessToList = validateAccessToList;
  private validateAccessToSingle = validateAccessToSingle;

  async createOne({ createMessageDto, credentials }: CreateService): Promise<Message> {
    try {
      const { roomID } = createMessageDto;

      const room = await this.roomsService.findOne({ query: { _id: roomID }, credentials});

      const newMessage = new this.messagesModel({
        ...createMessageDto,
        createdBy: credentials._id,
      });

      const messageValidate: any = newMessage;
      messageValidate.room = room;

      this.validateAccessToSingle({
        data: messageValidate,
        credentials,
        authzKey: 'room',
      });

      await newMessage.save();

      return newMessage;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findOne({ query, credentials }: FindOne): Promise<Message> {
    try {
      const message = await this.messagesModel
        .findOne(query)
        .populate({
          path: 'createdBy',
          select: '_id email title roles createdAt',
        })
        .populate('roomID');

      if (!message) {
        return Promise.reject({
          name: 'MessageNotFound',
          code: 404,
        });
      }

      const { validData } = this.validateAccessToSingle({
        data: message,
        credentials,
        authzKey: 'roomID',
      });

      return validData;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findMany({ query, credentials }: FindManyService): Promise<FindAll> {
    try {
      const newQuery: any = { ...query };
      const { limit, sortBy, content  } = query;
      const promises = [];

      if (content) {
        newQuery.content = { $regex: content, $options: 'gmi' };
      }

      const { sortingCondition, findingQuery, findAllQuery, hasPage } = buildFindingQuery({ query: newQuery });

      if (hasPage) {
        promises.push(
          this.messagesModel.find(findingQuery)
          .sort(sortingCondition)
          .limit(Number(limit))
          .populate({
            path: 'createdBy',
            select: '_id email title roles createdAt',
          })
          .populate('roomID'),
          this.messagesModel.countDocuments(findAllQuery),
        );
      }

      if (!hasPage) {
        promises.push(
          this.messagesModel.find(findingQuery)
          .sort(sortingCondition)
          .populate({
            path: 'createdBy',
            select: '_id email title roles createdAt',
          })
          .populate('roomID'),
          this.messagesModel.countDocuments(findingQuery),
        );
      }

      const [messages, totalCount] = await Promise.all(promises);

      if (messages.length === 0) {
        return {
          totalCount: 0,
          list: [],
          cursor: 'END',
        };
      }

      const { validData } =  this.validateAccessToList({
        data: messages,
        credentials,
        authzKey: 'roomID',
      });

      const nextCursor = getNextCursor({ data: messages, sortBy });

      return {
        totalCount,
        list: validData,
        cursor: nextCursor,
      };
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async updateOne({ query, credentials, updateMessageDto }: UpdateService): Promise<Message> {
    try {
      const message = await this.messagesModel
        .findOne(query)
        .populate('roomID');

      if (!message) {
        return Promise.reject({
          name: 'MessageNotFound',
          code: 404,
        });
      }

      this.validateAccessToSingle({
        data: message,
        credentials,
        authzKey: 'roomID',
      });

      const newMessage = await this.messagesModel.findOneAndUpdate(
        query,
        { $set: updateMessageDto },
        { upsert: false, new: true },
      );

      return newMessage;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async deleteOne({ query, credentials }: DeleteService): Promise<boolean> {
    try {
      const message = await this.messagesModel
        .findOne(query)
        .populate('roomID');

      if (!message) {
        return Promise.reject({
          name: 'MessageNotFound',
          code: 404,
        });
      }

      this.validateAccessToSingle({
        data: message,
        credentials,
        authzKey: 'roomID',
      });

      await this.messagesModel.findOneAndDelete(query);

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async deleteMany({ query, credentials }: DeleteService): Promise<boolean> {
    try {
      const messages = await this.messagesModel
        .find(query)
        .populate('roomID');

      if (!messages || messages.length === 0) {
        return Promise.reject({
          name: 'MessageNotFound',
          code: 404,
        });
      }

      const { validData, validDataLength } = this.validateAccessToList({
        data: messages,
        credentials,
        authzKey: 'roomID',
      });

      if (validDataLength < validData.length) {
        return Promise.reject({
          name: 'ValidationError',
          code: 403,
        });
      }

      await this.messagesModel.deleteMany(query);

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
