import * as mongoose from 'mongoose';

const { Types: { ObjectId } } = mongoose.Schema;

export const MessagesSchema = new mongoose.Schema({
  content: { type: String, required: true },
  roomID: { type: ObjectId, ref: 'Rooms' },
  createdBy: { type: ObjectId, ref: 'Users', required: true },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});

MessagesSchema.pre('findOneAndUpdate', function() {
  this.update({ }, { updatedAt: Date.now()});
});
