import { Controller, Post, Request, UseGuards, UsePipes, Param, Body, Get, Query, Put, Delete } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { ValidationPipe } from '../../middlewares/pipes/validation.pipe';
import { AuthGuard } from '@nestjs/passport';
import { checkControllerErrors } from '../../helpers/checkErrors';
import { MessagesService } from './messages.service';
import { CreateMessageDto, FindAllDto, UpdateMessageDto } from './types/messages.dto';
import { Message, FindAll } from './types/messages.interface';
import { FireBaseService } from '../firebase/firebase.service';
import { RoomsService } from '../rooms/rooms.service';
import { NotificationsService } from '../notifications/notifications.service';
import { ConfigService } from '../../configs/configs.service';

@ApiUseTags('rooms/:roomID/messages')
@Controller('rooms/:roomID/messages')
export class MessagesController {
  constructor(
    private readonly messagesService: MessagesService,
    private readonly firebaseService: FireBaseService,
    private readonly roomsService: RoomsService,
    private readonly notificationsService: NotificationsService,
    private readonly configService: ConfigService,
  ) { }

  @UsePipes(new ValidationPipe())
  @UseGuards(AuthGuard('jwt'))
  @Post()
  async createMessage(
    @Request() { user: credentials },
    @Param() { roomID },
    @Body() createMessageDto: CreateMessageDto,
  ): Promise<Message> {
    try {
      const newMessage = await this.messagesService.createOne(
        { createMessageDto: { ...createMessageDto, roomID },
        credentials,
      });

      await this.firebaseService.sendMessageToTopic({
        topic: `room-${newMessage.roomID}`,
        message: {
          messageID: newMessage._id.toString(),
          content: newMessage.content,
          click_action: `http://${this.configService.host}:8889/rooms/${roomID}`,
        },
        data: {
          roomID: newMessage.roomID.toString(),
          event: 'ADDED_MESSAGE',
          createdBy: credentials._id.toString(),
          createdAt: newMessage.createdAt.toDateString(),
          email: `${credentials.email}`,
        },
      });

      const room: any = await this.roomsService.findOne({
        query: { _id: newMessage.roomID },
        credentials },
      );

      if (room.members) {
        const promises = room.members.map(each => {
          if (each._id.toString() === credentials._id.toString()) {
            return;
          }

          return this.notificationsService.createOne({
            credentials,
            createNotification: {
              title: `New message from ${credentials.email}`,
              body: newMessage.content,
              clickAction: `http://${this.configService.host}:8889/rooms/${room._id}`,
              targetUID: each._id,
              type: 'ADDED_MESSAGE',
              roomID,
            },
          });
        });

        const notifications = await Promise.all(promises);

          // todos: get click action
        const sendMessagePromises = notifications.map((each: any) => {
          if (!each || each.targetUID.toString() === credentials._id.toString()) {
            return;
          }

          return this.firebaseService.sendMessageToTopic({
            topic: `user-${each.targetUID}`,
            notification: {
              title: each.title,
              body: each.body,
              click_action: each.clickAction,
            },
            data: {
              roomID: newMessage.roomID.toString(),
              event: 'ADDED_MESSAGE',
              isRead: 'false',
              notificationID: each._id.toString(),
              createdBy: credentials._id.toString(),
              createdAt: each.createdAt.toDateString(),
              email: `${credentials.email}`,
              targetUID: each.targetUID.toString(),
            },
          });
        });

        await Promise.all(sendMessagePromises);
      }

      return newMessage;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @UsePipes(new ValidationPipe())
  @UseGuards(AuthGuard('jwt'))
  @Get(':messageID')
  async findOne(
    @Request() { user },
    @Param() { roomID, messageID },
  ): Promise<Message> {
    try {
      const message = await this.messagesService.findOne({
        query: { _id: messageID, roomID },
        credentials: user,
      });

      return message;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @UsePipes(new ValidationPipe())
  @UseGuards(AuthGuard('jwt'))
  @Get()
  async findMany(
    @Request() { user },
    @Param() { roomID },
    @Query() findAllDto: FindAllDto,
  ): Promise<FindAll> {
    try {
      const messages = await this.messagesService.findMany({
        query: { ...findAllDto, roomID },
        credentials: user,
      });

      return messages;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @UsePipes(new ValidationPipe())
  @UseGuards(AuthGuard('jwt'))
  @Put(':messageID')
  async updateOne(
    @Request() { user: credentials },
    @Param() { roomID, messageID },
    @Body() updateMessageDto: UpdateMessageDto,
  ): Promise<Message> {
    try {
      const newMessage = await this.messagesService.updateOne({
        query: { _id: messageID, roomID },
        credentials,
        updateMessageDto,
      });

      await this.firebaseService.sendMessageToTopic({
        topic: `room-${newMessage.roomID}`,
        message:  {
          messageID: newMessage._id.toString(),
          content: newMessage.content,
          click_action: `http://${this.configService.host}:8889/rooms/${roomID}`,
        },
        data: {
          roomID: newMessage.roomID.toString(),
          event: 'UPDATED_MESSAGE',
          createdBy: credentials._id.toString(),
          isRead: 'false',
          createdAt: new Date().toString(),
          email: `${credentials.email}`,
        },
      });

      return newMessage;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @UsePipes(new ValidationPipe())
  @UseGuards(AuthGuard('jwt'))
  @Delete(':messageID')
  async deleteOne(
    @Request() { user: credentials },
    @Param() { roomID, messageID },
  ): Promise<boolean> {
    try {
      const isDeleted = await this.messagesService.deleteOne({
        query: { _id: messageID, roomID },
        credentials,
      });

      await this.firebaseService.sendMessageToTopic({
        topic: `room-${roomID}`,
        message: {
          messageID,
          content: '',
          click_action: `http://${this.configService.host}:8889/rooms/${roomID}`,
        },
        data: {
          roomID,
          event: 'DELETED_MESSAGE',
          createdBy: credentials._id.toString(),
          isRead: 'false',
          createdAt: new Date().toString(),
          email: `${credentials.email}`,
        },
      });

      return isDeleted;
    } catch (error) {
      checkControllerErrors(error);
    }
  }
}
