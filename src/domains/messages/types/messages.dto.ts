import { IsString, IsEnum, IsOptional, IsNumberString, IsMongoId } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Types } from 'mongoose';

export class CreateMessageDto {
  @IsString()
  @ApiModelProperty()
  readonly content: string;

  readonly roomID: Types.ObjectId;
}

export class UpdateMessageDto {
  @IsString()
  @ApiModelProperty()
  readonly content: string;
}

export class FindAllDto {
  @IsEnum(['updatedAt', 'createdAt'])
  @IsOptional()
  @ApiModelPropertyOptional()
  readonly sortBy?: string = 'createdAt';

  @IsEnum(['ASC', 'DESC'])
  @IsOptional()
  @ApiModelPropertyOptional()
  readonly sortDirection?: string = 'ASC';

  @IsNumberString()
  @ApiModelPropertyOptional()
  readonly limit: number | string = '10';

  @IsOptional()
  @ApiModelPropertyOptional()
  readonly cursor?: any;

  @IsString()
  @IsOptional()
  @ApiModelPropertyOptional()
  readonly content?: string;

  @IsMongoId()
  @IsOptional()
  @ApiModelPropertyOptional()
  readonly roomID?: string;
}
