import { Document, Types } from 'mongoose';
import { Credentials } from '../../../shared/validateAccess';
import { CreateMessageDto, UpdateMessageDto, FindAllDto } from './messages.dto';

export interface Message extends Document {
  content: string;
  roomID: Types.ObjectId;
  createdBy: Types.ObjectId;
  createdAt: Date;
  updatedAt: Date;
}

export interface FindManyService {
  query: FindAllDto;
  credentials: Credentials;
}

export interface DeleteService {
  query: object;
  credentials: Credentials;
}

export interface FindAll {
  totalCount: number;
  list: Message[];
  cursor: string;
}

export class CreateService {
  createMessageDto: CreateMessageDto;
  credentials: Credentials;
}

export class UpdateService {
  updateMessageDto: UpdateMessageDto;
  credentials: Credentials;
  query: object;
}
