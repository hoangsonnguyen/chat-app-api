import { Injectable } from '@nestjs/common';
import { credential, initializeApp, messaging } from 'firebase-admin';
import superagent = require('superagent');
import { ConfigService } from '../../configs/configs.service';
import {
  SendMessageToDevices, SendMessageToTopic, SubscribeTokenToTopic,
  UnsubscribeTokenToTopic,
} from './types/firebase.interface';
import { FirebaseConstants } from '../../constants/firebase';

const configService = new ConfigService();

@Injectable()
export class FireBaseService {
  constructor( ) {
    initializeApp({
      credential: credential.cert({
        projectId: configService.firebaseProjectId,
        clientEmail: configService.firebaseClientEmail,
        privateKey: configService.firebasePrivateKey.replace(/\\n/g, '\n'),
      }),
    });
  }

  // send message to devices by specific tokens
  async sendMessageToDevices({ tokens, notification, message }: SendMessageToDevices): Promise<boolean> {
    try {
      if (!notification && !message) {
        throw new Error('Notification or message is missing');
      }

      const payload: any = { };
      if (notification) {
        payload.notification = notification;
      }

      if (message) {
        payload.data = message;
      }

      const options = {
        priority: 'high',
        timeToLive: FirebaseConstants.timeToLive,
      };

      await messaging().sendToDevice(tokens, payload, options);

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  // send message
   async sendMessageToTopic({ topic, notification, message, data }: SendMessageToTopic): Promise<boolean> {
    try {
      if (!notification && !message) {
        throw new Error('Notification or message is missing');
      }

      const payload: any = { data };
      if (notification) {
        payload.notification = notification;
      }

      if (message) {
        payload.data = {
          ...payload.data,
          ...message,
        };
      }

      const options = {
        priority: 'high',
        timeToLive: FirebaseConstants.timeToLive,
      };

      await messaging().sendToTopic(topic, payload, options);

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async subscribeTokenToTopic({ tokens, topic }: SubscribeTokenToTopic): Promise<boolean> {
    try {
      if (!Array.isArray(tokens) || tokens.length === 0) {
        return;
      }

      await messaging().subscribeToTopic(tokens, topic);

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async unsubscribeTokenFromTopic({ tokens, topic }: UnsubscribeTokenToTopic): Promise<boolean> {
    try {
      if (!Array.isArray(tokens) || tokens.length === 0) {
        return;
      }

      await messaging().unsubscribeFromTopic(tokens, topic);

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getTokenPlatform(token: string) {
    try {
      const res = await superagent
        .get(configService.firebaseTokenInfoURI + token)
        .query({ details: true})
        .set('Authorization', 'key=' + configService.firebaseServerKey);

      return res.body.platform;
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
