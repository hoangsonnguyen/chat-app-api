interface MessageData {
  messageID: string;
  click_action: string;
  content: string;
}

interface NotificationData {
  body: string;
  title: string;
  click_action: string;
}

export interface SendMessageToDevices {
  tokens: string[];
  notification?: NotificationData;
  message?: MessageData;
}

export interface SendMessageToTopic {
  topic: string;
  notification?: NotificationData;
  message?: MessageData;
  data: FirebaseData;
}

interface FirebaseData {
  notificationID?: string;
  isRead?: string;
  event: string;
  roomID: string;
  createdAt: string;
  createdBy: string;
  email: string;
  targetUID?: string;
  targetEmail?: string;
}

export interface SubscribeTokenToTopic {
  tokens: string[];
  topic: string;
}

export interface UnsubscribeTokenToTopic {
  tokens: string[];
  topic: string;
}
