import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import {
  IsEmail, IsString, IsMongoId, IsOptional, IsNotEmpty, ArrayUnique,
  MaxLength, MinLength, IsEnum, IsNumberString, IsArray,
} from 'class-validator';
import { Types } from 'mongoose';

export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly email: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly password: string;
}

export class LoginDto {
  @ApiModelProperty()
  @IsEmail()
  readonly email: string;

  @ApiModelProperty()
  @IsString()
  @IsNotEmpty()
  readonly password: string;
}

export class ResetPasswordDto {
  @MaxLength(30)
  @MinLength(8)
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly password: string;
}

export class ChangePasswordDto {
  @MaxLength(30)
  @MinLength(8)
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly password: string;

  @MaxLength(30)
  @MinLength(8)
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly oldPassword: string;
}

export class InputEmailDto {
  @IsEmail()
  @ApiModelProperty()
  readonly email: string;
}

export class UpdateTopicDto {
  @IsString()
  @IsOptional()
  @ApiModelPropertyOptional()
  readonly topics?: string[];
}

export class FindAllUserDto {
  @IsOptional()
  @ApiModelPropertyOptional()
  readonly sortBy?: string = '_id';

  @IsEnum(['ASC', 'DESC'])
  @IsOptional()
  @ApiModelPropertyOptional()
  readonly sortDirection?: string = 'ASC';

  @IsNumberString()
  @IsOptional()
  @ApiModelPropertyOptional()
  readonly limit?: string = '10';

  @IsString()
  @IsOptional()
  @ApiModelPropertyOptional()
  readonly cursor?: string;

  @IsMongoId({ each: true })
  @ArrayUnique()
  @IsArray()
  @IsOptional()
  readonly ids?: Types.ObjectId[];

  @IsString()
  @IsOptional()
  @ApiModelPropertyOptional()
  readonly email?: string;
}

export class UpdateTokenDto {
  @IsString()
  @IsOptional()
  @ApiModelPropertyOptional()
  readonly token?: string;

  @IsEnum(['ANDROID', 'IOS', 'BROWSER'])
  @IsString()
  @ApiModelProperty()
  readonly platform: string;
}

export class InputTokenDto {
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly token: string;
}
