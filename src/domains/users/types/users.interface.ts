import { Document, Types } from 'mongoose';
import { Credentials } from '../../../shared/validateAccess';
import { ResetPasswordDto, ChangePasswordDto, UpdateTokenDto, InputTokenDto, UpdateTopicDto } from './users.dto';
import { FindAllUserDto } from './users.dto';

export interface User extends Document {
  email: string;
  password: string;
  oldPasswords: string[];
  changePasswordAt: number;
  firebaseToken: {
    browser: string[];
    android: string[];
    ios: string[];
  };
  topics: string[];
  createdAt: string;
  updatedAt: string;
}

// if want to use type of ObjectId, use Types.ObjectId from mongoose
export interface GetCurrentUserCredentials {
  email?: string;
  _id?: Types.ObjectId;
  googleID?: string;
}

export interface CurrentUserCredentials extends Document {
  email: string;
  password: string;
  firebaseToken: {
    browser: string[];
    android: string[];
    ios: string[];
  };
  topics: string[];
  oldPasswords: string[];
  changePasswordAt: number;
  createdAt: string;
  updatedAt: string;
}

export interface FindAll {
  totalCount: number;
  list: User[];
  cursor: string;
}

export interface FindMany {
  query: FindAllUserDto;
}

export interface UpdateTokenService {
  credentials: Credentials;
  updateTokenDto: UpdateTokenDto;
}

export interface GetOldTokenService {
  credentials: Credentials;
  platform: string;
}

export interface GetAllTokenService {
  credentials: Credentials;
  query: object;
}

export interface ChangePasswordService {
  query: object;
  credentials: Credentials;
  changePasswordDto: ChangePasswordDto;
}

export interface ResetPasswordService {
  query: object;
  credentials: Credentials;
  resetPasswordDto: ResetPasswordDto;
}

export interface GetAllTokenService {
  credentials: Credentials;
  query: object;
}

export interface UpdateFirebaseTokenService {
  credentials: Credentials;
  token: string;
}

export interface GetTokenByPlatformService {
  credentials: Credentials;
  platform: string;
}

export interface HandleFirebaseTokenService {
  tokenDto: InputTokenDto;
  credentials: Credentials;
}

export interface UpdateTopicService {
  query: object;
  updateTopicDto: UpdateTopicDto;
}
