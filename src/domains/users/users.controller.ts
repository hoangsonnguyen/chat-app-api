import { Get, Controller, Request, Post, Body, UseGuards, UsePipes,
  forwardRef, Inject, Query, Put } from '@nestjs/common';
import { UsersService } from './users.service';
import { checkControllerErrors } from '../../helpers/checkErrors';
import {
  CreateUserDto, ResetPasswordDto, InputTokenDto, LoginDto,
  ChangePasswordDto, FindAllUserDto, InputEmailDto,
} from './types/users.dto';
import { ApiUseTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from '../../middlewares/auth/auth.service';
import { ValidationPipe } from '../../middlewares/pipes/validation.pipe';
import { FindAll } from './types/users.interface';

@Controller()
@ApiUseTags()
export class UsersController {
  constructor(
    @Inject(forwardRef(() => AuthService))
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) { }

  @Post('auth/signup')
  @UsePipes(new ValidationPipe())
  async signup(@Body() createUserDto: CreateUserDto): Promise<any> {
    try {
      const user = await this.usersService.create(createUserDto);

      return await this.authService.generateJWT(user);
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Post('auth/login')
  @UseGuards(AuthGuard('local'))
  async login(@Request() { user }, @Body() _: LoginDto) {
    try {
      return await this.authService.generateJWT(user);
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  // an example for a protected route
  @Get('users/me')
  @UseGuards(AuthGuard('jwt'))
  async readUser(@Request() { user }) {
    try {
      const userData = await this.usersService.findOne({ _id: user._id});

      return userData;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Put('users/me/token/add')
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async updateToken(
    @Request() { user },
    @Body() tokenDto: InputTokenDto,
  ): Promise<boolean> {
    try {
      const isUpdated = await this.usersService.updateUserToken({
        tokenDto,
        credentials: user,
      });

      return isUpdated;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Put('users/me/token/remove')
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async removeToken(
    @Request() { user },
    @Body() tokenDto: InputTokenDto,
  ): Promise<boolean> {
    try {
      const { token } = tokenDto;
      const isUpdated = await this.usersService.deleteUserToken({
        token,
        credentials: user,
      });

      return isUpdated;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Put('users/me/password')
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async changePassword(
    @Request() { user },
    @Body() changePasswordDto: ChangePasswordDto,
  ): Promise<any> {
    try {
      const newUser = await this.usersService.changePassword({
        query: { _id: user._id },
        credentials: user,
        changePasswordDto,
      });

      return await this.authService.generateJWT(newUser);
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Put('users/me/password/reset')
  @UseGuards(AuthGuard('inactivate'))
  @UsePipes(new ValidationPipe())
  async resetPassword(
    @Request() { user },
    @Body() resetPasswordDto: ResetPasswordDto,
    @Query() token: InputTokenDto,
  ): Promise<any> {
    try {
      const newUser = await this.usersService.resetPassword({
        query: { _id: user._id },
        resetPasswordDto,
        credentials: user,
      });

      return this.authService.generateJWT(newUser);
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Post('users/me/password/reset')
  @UsePipes(new ValidationPipe())
  async sendEmailToResetPassword(
    @Body() inputEmailDto: InputEmailDto,
  ): Promise<boolean> {
    try {
      const isSent = await this.usersService.sendEmailToResetPassword(inputEmailDto);

      return isSent;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Get('users')
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async findMany(
    @Query() findAllUserDto: FindAllUserDto,
  ): Promise<FindAll> {
    try {
      const userData = await this.usersService.findMany({ query: findAllUserDto });

      return userData;
    } catch (error) {
      checkControllerErrors(error);
    }
  }
}
