import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  User, GetCurrentUserCredentials, CurrentUserCredentials,
  FindMany, ChangePasswordService, ResetPasswordService, FindAll,
  UpdateFirebaseTokenService, HandleFirebaseTokenService, UpdateTopicService,
} from './types/users.interface';
import { CreateUserDto, InputEmailDto } from './types/users.dto';
import * as bcrypt from 'bcrypt';
import { ConfigService } from '../../configs/configs.service';
import { buildFindingQuery, buildFindingQueryByObject } from '../../helpers/build';
import { getNextCursor, getTokens } from '../../helpers/get';
import { addMonth } from '../../helpers/time';
import { getHtmlForResettingPassword } from '../../helpers/getHTMLTemplate';
import { AuthService } from '../../middlewares/auth/auth.service';
import { FireBaseService } from '../firebase/firebase.service';
import { MailerService } from '@nest-modules/mailer';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel('Users') private readonly usersModel: Model<User>,
    private readonly configService: ConfigService,
    private readonly authService: AuthService,
    private readonly firebaseService: FireBaseService,
    private readonly mailerService: MailerService,
  ) { }

  async create(createUserDto: CreateUserDto): Promise<User> {
    try {
      const { email, password, ...createUser } = createUserDto;

      const changePasswordAt = addMonth(new Date(), 4);

      const hashPassword = await bcrypt.hash( password, this.configService.bcryptSalt);
      const createdUser = new this.usersModel({
        ...createUser,
        email,
        password: hashPassword,
        oldPasswords: [hashPassword],
        changePasswordAt: changePasswordAt.getTime(),
      });

      await createdUser.save();

      return createdUser;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getCurrentUserCredentials(query: GetCurrentUserCredentials): Promise<CurrentUserCredentials> {
    try {
      const user: any = await this.usersModel.findOne(query);

      return user;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findOne(query: object): Promise<User> {
    try {
      const user = await this.usersModel
      .findOne(query)
      .select([ '_id', 'email', 'changePasswordAt', 'createdAt']);

      if (!user) {
        return Promise.reject({
          name: 'UserNotFound',
          code: 404,
        });
      }

      delete user.password;

      return user;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findMany({ query }: FindMany): Promise<FindAll> {
    try {
      let newQuery: any = { ...query };
      const { limit, sortBy, email } = query;
      const promises = [];

      if (email) {
        newQuery.email = { $regex: email, $options: 'gmi' };
      }

      newQuery = buildFindingQueryByObject({
        query: newQuery,
        objectKeys: {
          ids: '_id',
        },
      });

      const { sortingCondition, findingQuery, findAllQuery, hasPage } = buildFindingQuery( { query: newQuery });

      if ( hasPage ) {
        promises.push(
          this.usersModel.find(findingQuery)
          .sort(sortingCondition)
          .limit(Number(limit))
          .select([ '_id', 'email', 'changePasswordAt', 'createdAt firebaseToken']),
          this.usersModel.countDocuments(findAllQuery),
        );
      }

      if (!hasPage) {
        promises.push(
          this.usersModel.find(findingQuery)
          .sort(sortingCondition)
          .select([ '_id', 'email', 'changePasswordAt', 'createdAt firebaseToken']),
          this.usersModel.countDocuments(findingQuery),
        );
      }

      const [users, totalCount ] = await Promise.all(promises);

      if (!users || !users.length || !totalCount) {
        return {
          totalCount: 0,
          list: [],
          cursor: 'END',
        };
      }

      const userData = users.map(user => {
        delete user.password;
        return user;
      });

      const nextCursor = getNextCursor({ data: users, sortBy });

      return {
        totalCount,
        list: userData,
        cursor: nextCursor,
      };
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async addTopic({ query, updateTopicDto }: UpdateTopicService): Promise<boolean> {
    try {
      await this.usersModel.updateMany(
        query,
        { $addToSet: updateTopicDto },
        { upsert: false },
      );

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async removeTopic({ query, updateTopicDto }: UpdateTopicService): Promise<boolean> {
    try {
      await this.usersModel.updateMany(
        query,
        { $pullAll: updateTopicDto },
        { upsert: false },
      );

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async changePassword({ query, credentials, changePasswordDto }: ChangePasswordService): Promise<User> {
    try {
      const { password, oldPassword } = changePasswordDto;

      const user = await this.usersModel.findOne(query);

      if (!user) {
        return Promise.reject({
          name: 'UserNotFound',
          code: 404,
        });
      }

      if (credentials._id.toString() !== user._id.toString) {
        return Promise.reject({
          code: 403,
          name: 'ValidationError',
        });
      }

      const isRightPassword = await bcrypt.compare(oldPassword, user.password);

      if (!isRightPassword) {
        return Promise.reject({
          name: 'OldPasswordNotCorrect',
          code: 400,
        });
      }

      const promises = user.oldPasswords.map(each => {
        return bcrypt.compare(password, each);
      });

      const comparingOldPassword = await Promise.all(promises);

      const isOldPassword = comparingOldPassword.includes(true);

      if (isOldPassword) {
        return Promise.reject({
          name: 'OldPassword',
          code: 400,
        });
      }

      const hashPassword = await bcrypt.hash( password, this.configService.bcryptSalt);

      const changePasswordAt = addMonth(new Date(), 4);

      const newUser = await this.usersModel.findOneAndUpdate(
        query,
        {
          $set: {
            password: hashPassword,
            changePasswordAt: changePasswordAt.getTime(),
          },
          $addToSet: {
            oldPasswords: hashPassword,
          },
        },
        {
          upsert: false,
          new: true,
        },
      );

      delete newUser.password;

      return newUser;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async resetPassword({ query, credentials, resetPasswordDto }: ResetPasswordService): Promise<User> {
    try {
      const { password } = resetPasswordDto;

      const user = await this.usersModel.findOne(query);

      if (!user) {
        return Promise.reject({
          name: 'UserNotFound',
          code: 404,
        });
      }

      if (credentials._id.toString() !== user._id.toString) {
        return Promise.reject({
          code: 403,
          name: 'ValidationError',
        });
      }

      const promises = user.oldPasswords.map(each => {
        return bcrypt.compare(password, each);
      });

      const comparingOldPassword = await Promise.all(promises);

      const isOldPassword = comparingOldPassword.includes(true);

      if (isOldPassword) {
        return Promise.reject({
          name: 'OldPassword',
          code: 400,
        });
      }

      const hashPassword = await bcrypt.hash( password, this.configService.bcryptSalt);

      const changePasswordAt = addMonth(new Date(), 4);

      const newUser = await this.usersModel.findOneAndUpdate(
        query,
        {
          $set: {
            password: hashPassword,
            changePasswordAt: changePasswordAt.getTime(),
          },
          $addToSet: {
            oldPasswords: hashPassword,
          },
        },
        {
          upsert: false,
          new: true,
        },
      );

      delete newUser.password;

      return newUser;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async sendEmailToResetPassword(inputEmailDto: InputEmailDto): Promise<boolean> {
    try {
      const { email } = inputEmailDto;

      const user = await this.usersModel.findOne({ email });

      if (!user) {
        return Promise.reject({
          name: 'UserNotFound',
          code: 404,
        });
      }

      const { accessToken } = await this.authService.generateInactivateJWT({ _id: user._id });

      // todos: change href (front-end)

      await this.mailerService.sendMail({
        to: user.email,
        subject: 'Chat App',
        text: 'Click to reset your password',
        html: getHtmlForResettingPassword(accessToken),
      });

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async updateUserToken({ tokenDto, credentials }: HandleFirebaseTokenService): Promise<boolean> {
    try {
      const { token } = tokenDto;

      const [platform, user] = await Promise.all([
        this.firebaseService.getTokenPlatform(token),
        this.usersModel.findOne({ _id: credentials._id }),
      ]);

      const lowerPlatform = platform.toLowerCase();

      const { firebaseToken, topics } = user;

      if (firebaseToken && firebaseToken[lowerPlatform] && firebaseToken[lowerPlatform].includes(token)) {
        return true;
      }

      firebaseToken[lowerPlatform].push(token);

      await this.usersModel.updateOne(
        { _id: credentials._id },
        {  firebaseToken },
        { upsert: false },
      );

      const promises = [this.firebaseService.subscribeTokenToTopic({
          tokens: [token],
          topic: `user-${credentials._id}`,
        })];

      if (topics) {
        topics.map(each => {
          promises.push(this.firebaseService.subscribeTokenToTopic({
            tokens: [token],
            topic: each,
          }));
        });

        await Promise.all(promises);
      }

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async deleteUserToken({ token, credentials }: UpdateFirebaseTokenService) {
    try {
      const [platform, user] = await Promise.all([
        this.firebaseService.getTokenPlatform(token),
        this.usersModel.findOne({ _id: credentials._id }),
      ]);

      const lowerPlatform = platform.toLowerCase();

      const { firebaseToken, topics } = user;

      if (!firebaseToken || !firebaseToken[lowerPlatform] || !firebaseToken[lowerPlatform].includes(token)) {
        return true;
      }

      firebaseToken[lowerPlatform] = firebaseToken[lowerPlatform].filter(each => !(each === token));

      await this.usersModel.updateOne(
        { _id: credentials._id },
        { firebaseToken },
        { upsert: false },
      );

      const promises = [this.firebaseService.unsubscribeTokenFromTopic({
          tokens: [token],
          topic: `user-${credentials._id}`,
        })];

      if (topics) {
        topics.map(each => {
          promises.push(this.firebaseService.unsubscribeTokenFromTopic({
            tokens: [token],
            topic: each,
          }));
        });

        await Promise.all(promises);
      }

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getAllTokens(query: object): Promise<string[]> {
    try {
      const users = await this.usersModel.find(query);

      const tokens = getTokens({
        users,
        tokens: [],
      });

      return tokens;
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
