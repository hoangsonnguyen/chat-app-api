import * as mongoose from 'mongoose';

export const UsersSchema = new mongoose.Schema({
  email: { type: String, unique: true, sparse: true, required: true },
  password: String,
  firebaseToken: {
    browser: [{ type: String, required: true }],
    android: [{ type: String, required: true }],
    ios: [{ type: String, required: true }],
  },
  topics: [{ type: String, required: true }],
  oldPasswords: [{ type: String, required: true }],
  changePasswordAt: Number,
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});

UsersSchema.pre('findOneAndUpdate', function() {
  this.update({ }, { updatedAt: Date.now() });
});

UsersSchema.pre('updateMany', function() {
  this.update({ }, { updatedAt: Date.now() });
});
