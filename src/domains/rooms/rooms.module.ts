import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RoomsSchame } from './models/rooms.schema';
import { RoomsController } from './rooms.controller';
import { RoomsService } from './rooms.service';
import { UsersModule } from '../users/users.module';
import { FireBaseModule } from '../firebase/firebase.module';
import { NotificationsModule } from '../notifications/notifications.module';

const RoomsModel =  MongooseModule.forFeature([{ name: 'Rooms', schema: RoomsSchame }]);

@Module({
  imports: [
    RoomsModel,
    forwardRef(() => UsersModule),
    FireBaseModule,
    NotificationsModule,
  ],
  controllers: [RoomsController],
  providers: [RoomsService],
  exports: [RoomsService, RoomsModel],
})
export class RoomsModule { }
