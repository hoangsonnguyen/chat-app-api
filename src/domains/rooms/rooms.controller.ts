import { Controller, Get, Request, Param, Query, Put, Body, UseGuards, UsePipes, Post, Delete } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { Room, FindAll } from './types/rooms.interface';
import { FindAllDto, UpdateRoomDto, UpdateMembersDto, CreateRoomDto } from './types/rooms.dto';
import { checkControllerErrors } from '../../helpers/checkErrors';
import { RoomsService } from './rooms.service';
import { AuthGuard } from '@nestjs/passport';
import { ValidationPipe } from '../../middlewares/pipes/validation.pipe';
import { UsersService } from '../users/users.service';
import { FireBaseService } from '../firebase/firebase.service';
import { NotificationsService } from '../notifications/notifications.service';
import { ConfigService } from '../../configs/configs.service';
import { getTokens } from '../../helpers/get';

@ApiUseTags('rooms')
@Controller('rooms')
export class RoomsController {
  constructor(
    private readonly roomsService: RoomsService,
    private readonly usersService: UsersService,
    private readonly firebaseService: FireBaseService,
    private readonly notificationsService: NotificationsService,
    private readonly configService: ConfigService,
  ) { }
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  @Post()
  async createOne(
    @Request() { user: credentials },
    @Body() createRoomDto: CreateRoomDto) {
    try {
      const room = await this.roomsService.createOne({
        createRoomDto,
        credentials,
      });

      if (room.users) {
        const createNotificationPromises = room.userData.map(each => {
          if (each._id.toString() === credentials._id.toString()) {
            return;
          }

          return this.notificationsService.createOne({
            credentials,
            createNotification: {
              title: 'You were added to a room',
              body: `${credentials.email} added you to a room`,
              clickAction: `http://${this.configService.host}:8889/rooms/${room._id}`,
              type: 'ADDED_ROOM',
              targetUID: each._id,
              roomID: room._id,
              targetEmail: each.email,
            },
          });
        });

        const notifications = await Promise.all(createNotificationPromises);

        const sendNotificationPromises = notifications.map((each: any) => {
          if (!each || each.targetUID.toString() === credentials._id.toString()) {
            return;
          }

          return this.firebaseService.sendMessageToTopic({
            topic: `user-${each.targetUID}`,
            notification: {
              title: each.title,
              body: each.body,
              click_action: each.clickAction,
            },
            data: {
              notificationID: each._id.toString(),
              event: each.type,
              isRead: each.isRead.toString(),
              roomID: each.roomID.toString(),
              createdBy: each.createdBy.toString(),
              createdAt: each.createdAt.toDateString(),
              email: `${credentials.email}`,
              targetUID: each.targetUID.toString(),
              targetEmail: each.targetEmail,
            },
          });
        });

        const tokens = await this.usersService.getAllTokens({ _id: { $in: room.users } });

        await Promise.all([
          this.usersService.addTopic({
            query: { _id: { $in: room.users } },
            updateTopicDto: { topics: [`room-${room._id}`] },
          }),
          this.firebaseService.subscribeTokenToTopic({
            tokens,
            topic: `room-${room._id}`,
          ...sendNotificationPromises,
          }),
        ]);
      }

      return room;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  @Get(':roomID')
  async findOne(
    @Request() { user },
    @Param() { roomID },
  ): Promise<Room> {
    try {
      const room = await this.roomsService.findOne({
        query: { _id: roomID },
        credentials: user,
      });

      return room;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  @Get()
  async findMany(
    @Request() { user },
    @Query() findAllDto: FindAllDto,
  ): Promise<FindAll> {
    try {
      const roomsData = await this.roomsService.findMany({
        query: findAllDto,
        credentials: user,
      });

      return roomsData;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  @Put(':roomID')
  async updateOne(
    @Request() { user },
    @Param() { roomID },
    @Body() updateRoomDto: UpdateRoomDto,
  ): Promise<boolean> {
    try {
      const isUpdated = await this.roomsService.updateOne(
        { query: { _id: roomID },
        credentials: user,
        updateRoomDto,
      });

      return isUpdated;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  @Put(':roomID/members/add')
  async addMembers(
    @Request() { user: credentials },
    @Param() { roomID },
    @Body() updateMembersDto: UpdateMembersDto,
  ): Promise<boolean> {
    try {
      const { users } = updateMembersDto;

      if (!users.length) {
        return true;
      }

      const room = await this.roomsService.addMembers(
        { query: { _id: roomID },
        updateMembersDto,
      });

      const userData = await this.usersService.findMany({
          query: { ids: room.users },
        });

      const tokens = getTokens({
        users: userData.list,
        tokens: [],
      });

      let addedEmails: any = userData.list.reduce((value, each) => {
        if (users.includes(each._id.toString())) {
          return value.concat(each.email);
        }

        return value;
      }, []);

      addedEmails = addedEmails.join(', ');

      const createNotificationPromises = userData.list.map(each => {
        if (each._id.toString() === credentials._id.toString()) {
          return;
        }

        if (users.includes(each._id.toString())) {
          return this.notificationsService.createOne({
            credentials,
            createNotification: {
              title: `You were added to a room`,
              body: `${credentials.email} added you to a room`,
              clickAction: `http://${this.configService.host}:8889/rooms/${roomID}`,
              type: 'ADDED_ROOM',
              targetUID: each._id,
              roomID,
              targetEmail: each.email,
            },
          });
        }

        return this.notificationsService.createOne({
          credentials,
          createNotification: {
            title: `${addedEmails} was added to a room`,
            body: `${credentials.email} added you to a room`,
            clickAction: `http://${this.configService.host}:8889/rooms/${roomID}`,
            type: 'ADDED_ROOM',
            targetUID: each._id,
            roomID,
            targetEmail: each.email,
          },
        });
      });

      const notifications = await Promise.all(createNotificationPromises);

      const sendNotificationPromises = notifications.map(each => {
        if (!each || each.targetUID.toString() === credentials._id.toString()) {
          return;
        }

        return this.firebaseService.sendMessageToTopic({
          topic: `user-${each.targetUID}`,
          notification: {
            title: each.title,
            body: each.body,
            click_action: each.clickAction,
          },
          data: {
            notificationID: each._id.toString(),
            event: each.type,
            isRead: each.isRead.toString(),
            roomID: each.roomID.toString(),
            createdBy: each.createdBy.toString(),
            createdAt: each.createdAt.toDateString(),
            email: `${credentials.email}`,
            targetUID: each.targetUID.toString(),
            targetEmail: each.targetEmail,
          },
        });
      });

      await Promise.all([
        this.usersService.addTopic({
          query: { _id: { $in: users } },
          updateTopicDto: { topics: [`room-${roomID}`] },
        }),
        this.firebaseService.subscribeTokenToTopic({
          tokens,
          topic: `room-${roomID}`,
        }),
        ...sendNotificationPromises,
      ]);

      return true;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  @Put(':roomID/members/remove')
  async removeMembers(
    @Request() { user },
    @Param() { roomID },
    @Body() updateMembersDto: UpdateMembersDto,
  ): Promise<boolean> {
    try {
      const { users } = updateMembersDto;

      if (!users.length) {
        return true;
      }

      const isRemoved = await this.roomsService.removeMembers(
        { query: { _id: roomID },
        credentials: user,
        updateMembersDto,
      });

      const tokens = await this.usersService.getAllTokens({ _id: { $in: users } });

      await Promise.all([
        this.firebaseService.unsubscribeTokenFromTopic({
          tokens,
          topic: `room-${roomID}`,
        }),
        this.usersService.removeTopic({
          query: { _id: { $in: users } },
          updateTopicDto: { topics: [`room-${roomID}`] },
        }),
      ]);

      return isRemoved;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  @Delete(':roomID')
  async deleteOne(
    @Request() { user },
    @Param() { roomID },
  ): Promise<boolean> {
    try {
      const isDeleted = await this.roomsService.deleteOne(
        { query: { _id: roomID },
        credentials: user,
      });

      return isDeleted;
    } catch (error) {
      checkControllerErrors(error);
    }
  }
}
