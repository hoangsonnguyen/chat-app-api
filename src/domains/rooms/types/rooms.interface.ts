import { Document, Types } from 'mongoose';
import { CreateRoomDto, UpdateRoomDto } from './rooms.dto';
import { Credentials } from '../../../shared/validateAccess';

export interface Room extends Document {
  name: string;
  users: Types.ObjectId[];
  createdBy: Types.ObjectId;
  createdAt: Date;
  updatedAt: Date;
}

export interface CreateRoomService {
  createRoomDto: CreateRoomDto;
  credentials: Credentials;
}

export interface UpdateRoomService {
  updateRoomDto: UpdateRoomDto;
  credentials: Credentials;
  query: object;
}

export interface FindManyService {
  query: object;
  credentials: Credentials;
}

export interface DeleteService {
  query: object;
  credentials: Credentials;
}

export interface FindAll {
  totalCount: number;
  list: Room[];
  cursor: string;
}
