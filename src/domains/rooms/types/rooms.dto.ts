import { Types } from 'mongoose';
import { IsString, IsOptional, IsEnum, IsMongoId, ArrayUnique, IsNumberString, IsNotEmpty } from 'class-validator';
import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

export class CreateRoomDto {
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  readonly name: string;

  @IsMongoId({ each: true })
  @ArrayUnique()
  @IsOptional()
  @ApiModelPropertyOptional()
  readonly users: Types.ObjectId[];
}

export class UpdateRoomDto {
  @IsString()
  @IsOptional()
  @ApiModelPropertyOptional()
  readonly name?: string;
}

export class UpdateMembersDto {
  @IsMongoId({ each: true })
  @ArrayUnique()
  @ApiModelProperty()
  readonly users: Types.ObjectId[] = [];
}

export class FindAllDto {
  @IsEnum(['updatedAt', 'createdAt', 'name'])
  @IsOptional()
  @ApiModelPropertyOptional()
  readonly sortBy?: string = 'updatedAt';

  @IsEnum(['ASC', 'DESC'])
  @IsOptional()
  @ApiModelPropertyOptional()
  readonly sortDirection?: string = 'ASC';

  @IsNumberString()
  @ApiModelPropertyOptional()
  readonly limit: number | string = '10';

  @IsOptional()
  @ApiModelPropertyOptional()
  readonly cursor?: any;

  @IsMongoId({ each: true })
  @ArrayUnique()
  @IsOptional()
  readonly users?: Types.ObjectId[];
}
