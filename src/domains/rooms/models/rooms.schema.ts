import * as mongoose from 'mongoose';

const { Types: { ObjectId } } = mongoose.Schema;

export const RoomsSchame = new mongoose.Schema({
  name: { type: String, required: true },
  users: [{ type: ObjectId, ref: 'Users', required: true }],
  createdBy: { type: ObjectId, ref: 'Users', required: true },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});

RoomsSchame.pre('findOneAndUpdate', function() {
  this.update({ }, { updatedAt: Date.now()});
});
