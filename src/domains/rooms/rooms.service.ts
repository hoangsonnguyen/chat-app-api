import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Room, FindManyService, CreateRoomService, FindAll, UpdateRoomService, DeleteService } from './types/rooms.interface';
import { validateAccessToSingle } from '../../shared/validateAccess';
import { buildFindingQuery, buildRoomFindingQuery } from '../../helpers/build';
import { getNextCursor } from '../../helpers/get';
import { FindOne } from '../../shared/interface';
import { UsersService } from '../users/users.service';
import { FireBaseService } from '../firebase/firebase.service';

@Injectable()
export class RoomsService {
  constructor(
    @InjectModel('Rooms') private readonly roomsModel: Model<Room>,
    @Inject(forwardRef(() => UsersService)) private readonly usersService: UsersService,
    private readonly firebaseService: FireBaseService,
  ) { }

  private validateAccessToSingle = validateAccessToSingle;

  async createOne({ createRoomDto, credentials }: CreateRoomService) {
    try {
      const { users: userIDs = [] } = createRoomDto;

      const users = await  this.usersService.findMany({ query: { ids: userIDs } });

      if (users.list.length < userIDs.length) {
        return Promise.reject({
          name: 'UserNotFound',
          code: 404,
        });
      }

      const room = new this.roomsModel({
        ...createRoomDto,
        createdBy: credentials._id,
      });

      await room.save();

      const userData = users.list.map(each => {
        return {
          _id: each._id,
          email: each.email,
        };
      });

      const newRoom: any = room;
      newRoom.userData = userData;

      return newRoom;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findMany({ query, credentials }: FindManyService): Promise<FindAll> {
    try {
      let newQuery: any = { ...query };
      const { limit, sortBy  } = newQuery;
      let roomData: any = { };

      newQuery = buildRoomFindingQuery(newQuery);

      const { sortingCondition, findingQuery, findAllQuery, hasPage } = buildFindingQuery({ query: newQuery });

      if (hasPage) {
        [roomData] = await this.roomsModel.aggregate([
          {
            $facet: {
              rooms: [
                { $match: findingQuery },
                { $sort: sortingCondition },
                { $limit: Number(limit) },
                {
                  $lookup: {
                    from: 'messages',
                    let: { id: '$_id' },
                    as: 'lastMessage',
                    pipeline: [
                      { $match: { $expr: { $eq: ['$$id', '$roomID'] }} },
                      { $sort: { createdAt: -1 } },
                      { $limit: 1 },
                    ],
                  },
                },
                {
                  $unwind: {
                    path: '$lastMessage',
                    preserveNullAndEmptyArrays: true,
                  },
                },
                {
                  $lookup: {
                    from: 'users',
                    let: { users: '$users' },
                    pipeline: [
                      { $match: { $expr: { $in: ['$_id', '$$users'] }} },
                      { $project: { _id: 1, email: 1 } },
                    ],
                    as: 'members',
                  },
                },
              ],
              totalCount: [
                { $match: findAllQuery },
                { $count: 'count' },
              ],
            },
          },
        ]);
      }

      if (!hasPage) {
        [roomData] = await this.roomsModel.aggregate([
          {
            $facet: {
              rooms: [
                { $match: findingQuery },
                { $sort: sortingCondition },
                {
                  $lookup: {
                    from: 'messages',
                    let: { id: '$_id' },
                    as: 'lastMessage',
                    pipeline: [
                      { $match: { $expr: { $eq: ['$$id', '$roomID'] }} },
                      { $sort: { createdAt: -1 } },
                      { $limit: 1 },
                    ],
                  },
                },
                {
                  $unwind: {
                    path: '$lastMessage',
                    preserveNullAndEmptyArrays: true,
                  },
                },
                {
                  $lookup: {
                    from: 'users',
                    let: { users: '$users' },
                    pipeline: [
                      { $match: { $expr: { $in: ['$_id', '$$users'] }} },
                      { $project: { _id: 1, email: 1 } },
                    ],
                    as: 'members',
                  },
                },
              ],
              totalCount: [
                { $match: findAllQuery },
                { $count: 'count' },
              ],
            },
          },
        ]);
      }

      const { rooms, totalCount } = roomData;

      if (!rooms || !totalCount[0] || !totalCount[0].count) {
        return {
          totalCount: 0,
          list: [],
          cursor: 'END',
        };
      }

      const nextCursor = getNextCursor({ data: rooms, sortBy });

      return {
        totalCount: totalCount[0].count,
        list: rooms,
        cursor: nextCursor,
      };
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findOne({ query, credentials }: FindOne): Promise<Room> {
    try {
      const newQuery = buildRoomFindingQuery(query);

      const [room] = await this.roomsModel.aggregate([
        { $match: newQuery },
        {
          $lookup: {
            from: 'messages',
            let: { id: '$_id' },
            as: 'lastMessage',
            pipeline: [
              { $match: { $expr: { $eq: ['$$id', '$roomID'] }} },
              { $sort: { createdAt: -1 } },
              { $limit: 1 },
            ],
          },
        },
        {
          $unwind: {
            path: '$lastMessage',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: 'users',
            let: { users: '$users' },
            pipeline: [
              { $match: { $expr: { $in: ['$_id', '$$users'] }} },
              { $project: { _id: 1, email: 1 } },
            ],
            as: 'members',
          },
        },
      ]);

      if (!room) {
        return Promise.reject({
          name: 'RoomNotFound',
          code: 404,
        });
      }

      const { validData } = this.validateAccessToSingle({
        data: room,
        credentials,
      });

      return validData;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async updateOne({ query, credentials, updateRoomDto }: UpdateRoomService): Promise<boolean> {
    try {
      const room = await this.roomsModel.findOne(query);

      if (!room) {
        return Promise.reject({
          name: 'RoomNotFound',
          code: 404,
        });
      }

      this.validateAccessToSingle({
        data: room,
        credentials,
      });

      await this.roomsModel.findOneAndUpdate(
        query,
        { $set: updateRoomDto },
        { upsert: false },
      );

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async addMembers({ query, updateMembersDto }): Promise<Room> {
    try {
      const { users = [] } = updateMembersDto;

      const userData = await  this.usersService.findMany({ query: { ids: users } });

      if (userData.list.length < users.length) {
        return Promise.reject({
          name: 'UserNotFound',
          code: 404,
        });
      }

      const room = await this.roomsModel.findOne(query);

      if (!room) {
        return Promise.reject({
          name: 'RoomNotFound',
          code: 404,
        });
      }

      const newRoom = await this.roomsModel.findOneAndUpdate(
        query,
        { $addToSet: updateMembersDto },
        { upsert: false, new: true },
      );

      return newRoom;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async removeMembers({ query, credentials, updateMembersDto }): Promise<boolean> {
    try {
      const room = await this.roomsModel.findOne(query);

      if (!room) {
        return Promise.reject({
          name: 'RoomNotFound',
          code: 404,
        });
      }

      this.validateAccessToSingle({
        data: room,
        credentials,
      });

      await this.roomsModel.findOneAndUpdate(
        query,
        { $pullAll: updateMembersDto },
        { upsert: false },
      );

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async deleteOne({ query, credentials }: DeleteService): Promise<boolean> {
    try {
      const room = await this.roomsModel.findOne(query);

      if (!room) {
        return Promise.reject({
          name: 'RoomNotFound',
          code: 404,
        });
      }

      this.validateAccessToSingle({
        data: room,
        credentials,
      });

      const { users = [] } = room;

      const tokens = await this.usersService.getAllTokens({
        query: { _id: { $in: users } },
        credentials,
      });

      await Promise.all([
        this.firebaseService.unsubscribeTokenFromTopic({
          tokens,
          topic: `room-${room._id}`,
        }),
        this.usersService.removeTopic({
          query: { _id: { $in: room.users } },
          updateTopicDto: { topics: [`room-${room._id}`] },
        }),
      ]);

      await this.roomsModel.findOneAndDelete(query);

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
