import { Injectable, ForbiddenException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  Notification, FindManyService, FindAll, CreateOneService, FindOneService, UpdateService,
  DeleteService,
} from './types/notifications.interface';
import { ValidateAccess } from '../../shared/validateAccess';
import { buildFindingQuery, buildFindingQueryByObject } from '../../helpers/build';
import { getNextCursor } from '../../helpers/get';

@Injectable()
export class NotificationsService {
  constructor(
    @InjectModel('Notifications') private readonly notificationsModel: Model<Notification>,
  ) { }

  private validateAccessToSingle = ({ data, credentials, authzKey}: ValidateAccess) => {
    if (!data || Array.isArray(data)) {
      return {
        validData: null,
      };
    }

    const { _id: userID } = credentials;

    const authzData = authzKey
      ? data[authzKey]
      : data;

    const isOwned = authzData.targetUID && userID === authzData.targetUID.toString();

    if (isOwned) {
      return {
        validData: data,
      };
    }

    throw new ForbiddenException('No permissions');
  }

  private validateAccessToList = ({ data, credentials, authzKey }: ValidateAccess) => {
    if (!data.length) {
      return {
        validData: [],
        validDataLength: 0,
      };
    }

    const { _id: userID } = credentials;

    let validDataLength = 0;

    const validData = data.map((each) => {
      const authzData = authzKey
        ? each[authzKey]
        : each;
      const isOwned = authzData.targetUID && userID === authzData.targetUID.toString();

      if (isOwned) {
        ++validDataLength;

        return each;
      }

      return null;
    });

    return {
      validData,
      validDataLength,
    };
  }

  async findMany({ query, credentials}: FindManyService): Promise<FindAll> {
    try {
      const { sortBy, limit } = query;
      const promises = [];

      const newQuery = buildFindingQueryByObject({
        query,
        objectKeys: {
          targetUIDs: 'targetUID',
        },
      });

      const { sortingCondition, findingQuery, findAllQuery, hasPage } = buildFindingQuery({ query: newQuery });
      if (hasPage) {
        promises.push(
          this.notificationsModel.find(findingQuery)
            .sort(sortingCondition)
            .limit(Number(limit)),
          this.notificationsModel.countDocuments(findAllQuery),
        );
      }

      if (!hasPage) {
        promises.push(
          this.notificationsModel
          .find(findingQuery)
          .sort(sortingCondition),
          this.notificationsModel.countDocuments(findAllQuery),
        );
      }

      const [notifications, totalCount] = await Promise.all(promises);

      if (!notifications || !notifications.length || !totalCount) {
        return {
          totalCount: 0,
          list: [],
          cursor: 'END',
        };
      }

      const { validData } = this.validateAccessToList({
        data: notifications,
        credentials,
      });

      const nextCursor = getNextCursor({ data: notifications, sortBy });

      return {
        totalCount,
        list: validData,
        cursor: nextCursor,
      };
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findOne({ query, credentials }: FindOneService): Promise<Notification> {
    try {
      const notification: Notification = await this.notificationsModel.findOne(query);

      if (!notification) {
        return Promise.reject({
          code: 404,
          name: 'NotificationNotFound',
        });
      }

      const { validData } = this.validateAccessToSingle({
        data: notification,
        credentials,
      });

      return validData;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async createOne({ createNotification, credentials }: CreateOneService): Promise<Notification> {
    try {
      const notification = new this.notificationsModel({
        ...createNotification,
        createdBy: credentials._id,
      });

      await notification.save();

      return notification;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async updateOne({ query, updateNotification, credentials }: UpdateService): Promise<Notification> {
    try {
      const notification = await this.notificationsModel.findOne(query);

      if (!notification) {
        return Promise.reject({
          code: 404,
          name: 'NotificationNotFound',
        });
      }

      this.validateAccessToSingle({
        data: notification,
        credentials,
      });

      const updatedNotification = await this.notificationsModel.updateOne(
        query,
        updateNotification,
        {
          new: true,
          upsert: false,
        },
      );

      return updatedNotification;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async deleteOne({ query, credentials }: DeleteService): Promise<boolean> {
    try {
      const notification = await this.notificationsModel.findOne(query);

      if (!notification) {
        return Promise.reject({
          code: 404,
          name: 'NotificationNotFound',
        });
      }

      this.validateAccessToSingle({
        data: notification,
        credentials,
      });

      await this.notificationsModel.deleteOne(query);

      return true;
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
