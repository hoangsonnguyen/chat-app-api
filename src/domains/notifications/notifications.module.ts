import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NotificationsController } from './notifications.controller';
import { NotificationsService } from './notifications.service';
import { NotificationsSchema } from './models/notifications.schema';
import { FireBaseModule } from '../firebase/firebase.module';
import { MailerModule } from '@nest-modules/mailer';

const NotificationsModel = MongooseModule.forFeature([{ name: 'Notifications', schema: NotificationsSchema }]);
@Module({
  imports: [
    NotificationsModel,
    FireBaseModule,
    MailerModule,
  ],
  controllers: [NotificationsController],
  providers: [NotificationsService],
  exports: [NotificationsService, NotificationsModel],
})

export class NotificationsModule { }
