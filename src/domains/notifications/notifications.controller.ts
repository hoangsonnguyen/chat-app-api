import { Get, Param, UsePipes, UseGuards, Request, Query, Controller, Put, Body } from '@nestjs/common';
import { checkControllerErrors } from '../../helpers/checkErrors';
import { ApiUseTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { NotificationsService } from './notifications.service';
import { Notification, FindAll } from './types/notifications.interface';
import { FindAllDto, UpdateNotificationDto } from './types/notifications.dto';
import { ValidationPipe } from '../../middlewares/pipes/validation.pipe';

@ApiUseTags('notifications')
@Controller('notifications')
export class NotificationsController {
  constructor(
    private readonly notificationsService: NotificationsService,
  ) { }

  @Get()
  @UsePipes(new ValidationPipe())
  @UseGuards(AuthGuard('jwt'))
  async findMany(
    @Request() { user},
    @Query() query: FindAllDto,
  ): Promise<FindAll> {
    try {
      const notification = await this.notificationsService.findMany({
        query: {
          ...query,
          targetUIDs: [user._id],
        },
        credentials: user,
      });
      return notification;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Get(':notificationID')
  @UseGuards(AuthGuard('jwt'))
  async findOne(
    @Request() { user },
    @Param() { notificationID },
  ): Promise<Notification> {
    try {
      const notification = await this.notificationsService.findOne({
        query: { _id: notificationID },
        credentials: user,
      });
      return notification;
    } catch (error) {
      checkControllerErrors(error);
    }
  }

  @Put(':notificationID')
  @UseGuards(AuthGuard('jwt'))
  async updateOne(
    @Request() { user },
    @Body() updateNotification: UpdateNotificationDto,
    @Param() { notificationID },
  ): Promise<Notification> {
    try {
      const notification = await this.notificationsService.updateOne({
        query: { _id: notificationID },
        credentials: user,
        updateNotification,
      });

      return notification;
    } catch (error) {
      checkControllerErrors(error);
    }
  }
}
