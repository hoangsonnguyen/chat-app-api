import * as mongoose from 'mongoose';

const { Types: { ObjectId } } = mongoose.Schema;

export const NotificationsSchema = new mongoose.Schema({
  type: { type: String, enum: ['ADDED_ROOM', 'ADDED_MESSAGE'], required: true},
  isRead: { type: Boolean, default: false},
  title: { type: String, required: true},
  body: { type: String, required: true},
  img: { type: String, required: false},
  roomID: { type: ObjectId, ref: 'Rooms', required: true },
  clickAction: { type: String, required: false},
  targetUID: { type: ObjectId, ref: 'Users', required: true },
  targetEmail: { type: String, default: null },
  createdBy: { type: ObjectId, ref: 'Users', required: true },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
});

NotificationsSchema.pre('findOneAndUpdate', function() {
  this.update({ }, { updatedAt: Date.now()});
});
