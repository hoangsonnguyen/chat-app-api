import { Document, Types } from 'mongoose';
import { FindAllDto, CreateNotificationDto, UpdateNotificationDto } from './notifications.dto';
import { Credentials } from 'src/shared/validateAccess';

export interface Notification extends Document {
  type: string;
  isRead: boolean;
  title: string;
  body: string;
  img: string;
  clickAction: string;
  targetUID: Types.ObjectId;
  targetEmail: string;
  roomID: Types.ObjectId;
  createdBy: Types.ObjectId;
  createdAt: Date;
}

export interface FindAll {
  totalCount: number;
  list: Notification[];
  cursor: string;
}

export interface FindManyService {
  query: FindAllDto;
  credentials: Credentials;
}

export interface FindOneService {
  query: object;
  credentials: Credentials;
}

export interface CreateOneService {
  createNotification: CreateNotificationDto;
  credentials: Credentials;
}

export interface UpdateService {
  query: object;
  credentials: Credentials;
  updateNotification: UpdateNotificationDto;
}

export interface DeleteService {
  query: object;
  credentials: Credentials;
}
