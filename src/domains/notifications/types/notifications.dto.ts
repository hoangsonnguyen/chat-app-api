import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsString, IsOptional, IsMongoId, IsBoolean, IsEnum, IsNumberString, IsArray, ArrayUnique } from 'class-validator';
import { Types } from 'mongoose';

export class CreateNotificationDto {
  @ApiModelProperty()
  @IsEnum(['ADDED_ROOM', 'ADDED_MESSAGE'])
  @IsString()
  readonly type: string;

  @ApiModelPropertyOptional()
  @IsBoolean()
  @IsOptional()
  readonly isRead?: boolean;

  @ApiModelProperty()
  @IsString()
  readonly title: string;

  @ApiModelPropertyOptional()
  @IsString()
  @IsOptional()
  readonly img?: string;

  @ApiModelProperty()
  @IsString()
  readonly body: string;

  @IsMongoId()
  @ApiModelProperty()
  readonly targetUID: Types.ObjectId;

  @IsMongoId()
  @ApiModelProperty()
  readonly roomID: Types.ObjectId;

  @ApiModelPropertyOptional()
  @IsString()
  @IsOptional()
  readonly clickAction?: string;

  @ApiModelPropertyOptional()
  @IsString()
  @IsOptional()
  readonly targetEmail?: string;
}

export class UpdateNotificationDto {
  @ApiModelPropertyOptional()
  @IsBoolean()
  @IsOptional()
  readonly isRead?: boolean;
}

enum sortBy { 'createdAt', 'targetUID', '_id', 'title'}
enum sortDirection { 'ASC', 'DESC' }

export class FindAllDto {
  @ApiModelPropertyOptional()
  @IsEnum(sortBy)
  @IsOptional()
  readonly sortBy?: sortBy | string = '_id';

  @ApiModelPropertyOptional()
  @IsEnum(sortDirection)
  @IsOptional()
  readonly sortDirection?: sortDirection | string = 'ASC';

  @ApiModelPropertyOptional()
  @IsNumberString()
  @IsOptional()
  readonly limit?: string = '10';

  @ApiModelPropertyOptional()
  @IsString()
  @IsOptional()
  readonly cursor?: string;

  @IsMongoId({ each: true })
  @ArrayUnique()
  @IsArray()
  @IsOptional()
  readonly targetUIDs?: string[];
}
