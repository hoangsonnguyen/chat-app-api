import { Injectable, forwardRef, Inject } from '@nestjs/common';
import { UsersService } from '../../domains/users/users.service';
import { AccessToken, TokenPayload } from './types/auth.interface';
import * as bcrypt from 'bcrypt';
import * as jsonwebtoken from 'jsonwebtoken';
import { ConfigService } from '../../configs/configs.service';

@Injectable()
export class AuthService {
  constructor(
    @Inject(forwardRef(() => UsersService))
    private readonly usersService: UsersService,
    private readonly configService: ConfigService,
  ) { }

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.usersService.getCurrentUserCredentials({ email });
    const isValid = user && await bcrypt.compare(password, user.password);

    if (isValid) {
      delete user.password;
      return user;
    }

    return null;
  }

  async generateInactivateJWT(user: any): Promise<AccessToken> {
    const payload = {
      _id: user._id.toString(),
    };

    const accessToken = await jsonwebtoken.sign(
      payload,
      this.configService.getJwtComfirmSecret,
      { expiresIn: '2d' },
    );

    return {
      accessToken,
    };
  }

  async generateJWT(user: any): Promise<AccessToken> {
    const payload: TokenPayload = {
      _id: user._id.toString(),
      changePasswordAt: user.changePasswordAt,
      email: user.email,
    };

    const accessToken = await jsonwebtoken.sign(
      payload,
      this.configService.jwtSecret,
      { expiresIn: '180d' },
    );

    return {
      accessToken,
    };
  }
}
