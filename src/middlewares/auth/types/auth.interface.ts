import { Types } from 'mongoose';

export interface ReqUser {
  _id: Types.ObjectId;
  email: string;
  changePasswordAt: number;
}

export interface AccessToken {
  accessToken: string;
}

export interface TokenPayload {
  _id: string;
  changePasswordAt: number;
  email: string;
}

export interface VerifyPayload {
  _id: Types.ObjectId;
}
