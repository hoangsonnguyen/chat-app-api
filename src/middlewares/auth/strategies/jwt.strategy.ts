import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '../../../configs/configs.service';
import { ReqUser } from '../types/auth.interface';
import { UsersService } from '../../../domains/users/users.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    configService: ConfigService,
    private readonly usersService: UsersService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.jwtSecret,
    });
  }

  // returned object is bound to req.user
  async validate(payload: any): Promise<ReqUser> {
    try {
      const userData = await this.usersService.getCurrentUserCredentials({ _id: payload._id});

      if (!userData) {
        throw new UnauthorizedException();
      }

      if (userData.changePasswordAt !== payload.changePasswordAt) {
        throw new UnauthorizedException('Your token is expired');
      }

      return {
        _id: payload._id,
        email: payload.email,
        changePasswordAt: userData.changePasswordAt,
      };
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
