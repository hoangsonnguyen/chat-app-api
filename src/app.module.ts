import { Module } from '@nestjs/common';
import { ConfigModule } from './configs/configs.module';
import { UsersModule } from './domains/users/users.module';
import { ConfigService } from './configs/configs.service';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './middlewares/auth/auth.module';
import { MessagesModule } from './domains/messages/messages.module';
import { RoomsModule } from './domains/rooms/rooms.module';
import { FireBaseModule } from './domains/firebase/firebase.module';
import { MailerModule } from '@nest-modules/mailer';

const DatabaseModule = MongooseModule.forRootAsync({
  useFactory: async (configService: ConfigService) => {
    return ({
      uri: configService.dbURI,
    });
  },
  inject: [ConfigService],
});

const EmailModule = MailerModule.forRootAsync({
  useFactory: async (configService: ConfigService) => {
    return ({
      transport: {
        host: configService.smtpHost,
        port: configService.smtpPort,
        auth: {
          user: configService.smtpUser,
          pass: configService.smtpPassword,
        },
      },
      defaults: {
        from: configService.smtpSenderDefault,
      },
    });
  },
  inject: [ConfigService],
});

@Module({
  imports: [
    ConfigModule,
    DatabaseModule,
    UsersModule,
    AuthModule,
    MessagesModule,
    RoomsModule,
    FireBaseModule,
    EmailModule,
  ],
})

export class AppModule { }
