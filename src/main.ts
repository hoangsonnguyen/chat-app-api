import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ConfigService } from './configs/configs.service';
const configService = new ConfigService();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  // build documents for APIs
  const options = new DocumentBuilder()
    .setTitle('chat app')
    .setDescription('The chat app APIs description')
    .setVersion('1.0')
    .addTag('ca')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(configService.port, configService.host);
  console.log(`app started at ${configService.host}:${configService.port}`);
}

bootstrap();
