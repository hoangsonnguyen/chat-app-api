import { User } from '../domains/users/types/users.interface';
interface GetTokens {
  users: User[] | any;
  tokens: string[];
}

export const getNextCursor = ({ data, sortBy }) => {
  let nextCursor = 'END';

  if (data.length) {
    nextCursor = data[data.length - 1].createdAt;
  }

  if (data.length && sortBy) {
    nextCursor = data[data.length - 1][sortBy];
  }

  return nextCursor;
};

export const getTokens = ({ users, tokens }: GetTokens) => {
  if (users.length === 0) {
    return tokens;
  }

  const keys = ['browser', 'android', 'ios'];

  for (const each of users) {
    if (!each.firebaseToken) {
      continue;
    }

    for (const key in each.firebaseToken) {
      if (each.firebaseToken[key] && keys.includes(key)) {
        tokens = tokens.concat(each.firebaseToken[key]);
      }
    }
  }

  return tokens;
};

export const getEmails = (users: User[]): string[] => {
  if (!users.length) {
    return [];
  }

  const emails = users.map(each => each.email);

  return emails;
};
