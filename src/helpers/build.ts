import { Types } from 'mongoose';

const { ObjectId } = Types;

export const buildFindingQuery = ({ query }) => {
  const { sortBy = '_id', limit, cursor, sortDirection, ...findingQuery } = query;
  const validDirection: number = sortDirection === 'ASC' ? 1 : -1;
  const hasPage = !!limit;
  const sortingCondition = { [sortBy]: validDirection };

  for (const key in findingQuery) {
    if (Array.isArray(findingQuery[key])) {
      findingQuery[key] = { $in: findingQuery[key] };
    }
  }

  const findAllQuery = { ...findingQuery };

  if (!limit) {
    return {
      findingQuery,
      sortingCondition,
      hasPage,
    };
  }

  if (!cursor) {
    return {
      sortingCondition,
      findingQuery,
      findAllQuery,
      hasPage,
    };
  }

  const condition = validDirection === 1 ? '$gt' : '$lt';
  findingQuery[sortBy] = { [condition]: cursor };

  return {
    sortingCondition,
    findingQuery,
    findAllQuery,
    hasPage,
  };
};

export const buildFindingQueryByObject = ({ query, objectKeys }) => {
  if (!objectKeys) {
    return query;
  }

  for (const key in objectKeys) {
    if (query[key]) {
      query[objectKeys[key]] = query[key];
      delete query[key];
    }
  }

  return query;
};

export const buildExhibitingSpace = ({ spaceExhibiting, spaceExhibitingLength, spaceExhibitingWidth }) => {
  if (spaceExhibitingLength) {
    spaceExhibiting.length = spaceExhibitingLength;
  }

  if (spaceExhibitingWidth) {
    spaceExhibiting.width = spaceExhibitingWidth;
  }

  return spaceExhibiting;
};

export const buildObjectIds = (IDs: string[]) => {
  if (!IDs || !Array.isArray(IDs)) {
    return [];
  }

  const result = IDs.map(id => ObjectId(id));

  return result;
};

interface FindingQuery {
  cursor?: any;
  sortBy?: string;
}

export const buildRoomFindingQuery = (query: FindingQuery) => {
  const keys = ['_id', 'users'];

  if (query.sortBy && query.cursor) {
    if (query.sortBy === '_id') {
      query.cursor = ObjectId(query.cursor);
    }

    if (query.sortBy === 'createdAt' || query.sortBy === 'updatedAt') {
      query.cursor = new Date(query.cursor);
    }
  }

  for (const key in query) {
    if (keys.includes(key)) {
      if (key === 'users') {
        query[key] = query[key].map(each => ObjectId(each));
        continue;
      }

      query[key] = ObjectId(query[key]);
    }
  }

  return query;
};

export const buildUserFindingQuery = (query: FindingQuery) => {
  const keys = ['ids'];

  if (query.sortBy && query.sortBy === '_id' && query.cursor) {
    query.cursor = ObjectId(query.cursor);
  }

  for (const key in query) {
    if (keys.includes(key)) {
      query[key] = query[key].map(each => ObjectId(each));
    }
  }

  return query;
};
