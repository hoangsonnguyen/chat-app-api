import { InternalServerErrorException, BadRequestException, ForbiddenException, NotFoundException } from '@nestjs/common';
import { notFoundErrors, badRequestErrors, forbiddenErrors } from '../constants/errors';
import { ConfigService } from '../configs/configs.service';

const configService = new ConfigService();
const checkControllerErrorsInDev = (error: any): void => {

  if (error.name === 'MongoError' && error.code === 11000) {
    throw new BadRequestException('Duplicated email/phone');
  }

  if (error.code === 403) {
    throw new ForbiddenException(forbiddenErrors[error.name]);
  }

  if (error.code === 404) {
    throw new NotFoundException(error);
  }

  if (error.code === 400) {
    throw new BadRequestException(error);
  }

  throw new InternalServerErrorException(error);
};

const checkControllerErrorsInProd = (error: any): void => {
  if (error.name === 'MongoError' && error.code === 11000) {
    throw new BadRequestException('Duplicated email/phone');
  }

  if (error.name === 'ValidationError' && error.code === 403) {
    throw new ForbiddenException('No permisisons');
  }

  if (error.code === 404) {
    throw new NotFoundException(notFoundErrors[error.name]);
  }

  if (error.code === 400) {
    throw new BadRequestException(badRequestErrors[error.name]);
  }

  throw new InternalServerErrorException('Something wrong happens');
};

export const checkControllerErrors = configService.nodeEnv === 'development'
    ? checkControllerErrorsInDev
    : checkControllerErrorsInProd;
