import * as request from 'request';
import { ConfigService } from '../configs/configs.service';
const configService = new ConfigService();

export interface CaptchaError {
  name: string;
  code: number;
}

export const checkCaptcha = (captcha: string): Promise<CaptchaError> => new Promise((resolve, reject) => {
  try {
    const url = `${configService.captchaVerifyUrl}?secret=${configService.captchaSecretKey}&response=${captcha}`;

    request(url, (err, response, body) => {
      if (err) {
        reject(err);
      }

      body = JSON.parse(body);

      if (!body.success || body.score < 0.4) {
        reject({
          name: 'Robot',
          code: 403,
        });
      }

      resolve();
    });
  } catch (err) {
    reject(err);
  }
});
