export const addMonth = (dateObject: Date, numMonths: number): Date => {
  dateObject.setMonth(dateObject.getMonth() + numMonths);

  return dateObject;
};
