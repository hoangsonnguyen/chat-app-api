import { ConfigService } from '../configs/configs.service';

const configService = new ConfigService();

export const getHtmlInSignUpUser = (token: string) => {
  return `<a href="http://${configService.host}:${configService.port}/auth/signup/activate/${token}">CONFIRM</a>`;
};

// todos: get URL to move user to update user's information page
export const getHtmlInInviteMembers = (token: string, organizationID: string) => {
  return `<a href="http://${configService.host}:${configService.port}/auth/signup/${token}">Sign Up by Email</a>`
    + `<a href="https://accounts.google.com/o/oauth2/auth?scope=email profile&redirect_uri=`
    + `http://${configService.host}:${configService.port}/auth/google/callback&response_type=code&client_`
    + `id=500635272349-1lqphc4cq4ree2pdpi096mfajc4hph0f.apps.googleusercontent.com&approval_prompt=force`
    + `&state=${organizationID}">Sign Up by Google</a>`;
};

// todos: get URL to move user to reset password page
export const getHtmlForResettingPassword = (token: string) => {
  return `<a href="http://${configService.host}:${configService.port}/users/me/password/reset/`
  + `?token=${token}">Get new password</a>`;
};
