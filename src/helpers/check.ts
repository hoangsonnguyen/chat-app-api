export const checkStringInObjectIDs = (neededCheck, requiredChecks): boolean => {
  if (!neededCheck || neededCheck.length === 0 || !Array.isArray(requiredChecks)) {
    return false;
  }

  for (const requiredCheck of requiredChecks) {
    if (neededCheck.toString() === requiredCheck.toString()) {
      return true;
    }
  }

  return false;
};

export const checkArrayObjectIDs = ({ requireIDs, neededCheckIDs }) => {
  const hasRequireIDs = !!requireIDs && Array.isArray(requireIDs) && requireIDs.length !== 0;
  const hasNeededCheckIDs = !!neededCheckIDs || Array.isArray(neededCheckIDs);

  if (!hasNeededCheckIDs || !hasRequireIDs) {
    return false;
  }

  for (const neededCheckID of neededCheckIDs) {
    let isSame = true;

    for (const requireID of requireIDs) {
      isSame = neededCheckID.toString() === requireID.toString();

      if (isSame) {
        break;
      }
    }

    if (!isSame) {
      return false;
    }
  }

  return true;
};
