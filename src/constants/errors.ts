export const notFoundErrors = {
  UserNotFound: 'User not found',
  RoomNotFound: 'Room not found',
  MessageNotFound: 'Message not found',
};

export const badRequestErrors = {
  InvalidParams: 'Params are invalid',
  OldPassword: 'You used to use this password, please enter new password!',
  // tslint:disable-next-line:max-line-length
  PasswordNotCorrect: 'Password must contain at least 1 lowercase alphabetical character, 1 uppercase alphabetical character, 1 numeric character and 1 special character',
  OldPasswordNotCorrect: 'Your password is not correct!',
  UserAlreadyExisted: 'This user is already exist',
};

export const forbiddenErrors = {
  ValidationError: 'No permisisons',
  Robot: 'Please verify by using recaptcha',
};
